# kosmo-galton

Here is an implementation of a Galton Board in TypeScript/JavaScript.  You can run it here: <https://aquarichy.gitlab.io/kosmo-galton/>

To learn more about Galton Boards, visit:
<https://www.compadre.org/osp/EJSS/3965/109.htm>

This project uses:

- TypeScript
- podman to build an OCI container

## Features

- step-by-step visualization
- "histogram"
- run modes
    - Step: do a run of the Galton Board step-by-step
    - Run Once: do an automated run of the Galton Board with each step taking a set interval
    - Run Many: do multiple runs of the Galton Board, building a histogram of the end position (should follow a normal distribution)
- options
    - Height: number of rows == number of end positions
    - Step Duration (ms): set how long intervals between steps on automated runs will take
        - &gt;0: uses setInterval; overhead means that very short intervals (e.g. 1ms) will take longer than expected
        - =0: runs **much** faster using recursion (BUT then we have a limit on how many iterations we can do :()
    - Show board: if you're doing many iterations very fast, you can hide the board itself, greatly speeding the process, and just focus on the histogram.
    - Show histogram + iteration progress: if you're doing many iterations, you can prevent in-progress updates to the histogram and iteration counter, greatly speeding the process.
- see time-elapsed for automated runs (Run Once, Run Many)

## Performance

Iteration size: Time complexity O(n)

| Height | Step Duration | Iterations | Show Board | Show Progress | Time Elapsed |
| ---:   | ---:          | ---:       | ---        | ---           | ---:         |
|    100 |             0 |      20000 | False      | False         |         98ms |
|    100 |             0 |      10000 | False      | False         |         53ms |
|    100 |             0 |       1000 | False      | False         |          5ms |
|    100 |             0 |       1000 | False      | True          |        834ms |
|    100 |             0 |       1000 | True       | False         |     19,769ms |
|    100 |             0 |       1000 | True       | True          |     45,407ms |

## Todo

- [ ] replace recursion with looping to allow us to go beyond 20,000 iterations

## Author

Author: Richard Schwarting <aquarichy@gmail.com> © 2023

License: GPLv3+
