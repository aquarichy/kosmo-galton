INSTALL_DIR=/var/www/public_html
OCI_BIN=podman
OCI_TAG=localhost/galton
OCI_FILE=oci/Containerfile.httpd
APP_NAME=galton
-include vars.mk # override above variables for local configuration

check: .eslint-passed .stylelint-passed

.eslint-passed: html/script.js
	npx eslint $^
	touch $@

.stylelint-passed: html/style.css
	npx stylelint $^
	touch $@

html/script.js: src/script.ts
	cd src && tsc --outDir ../html/

.make_install: html/script.js html/style.css html/index.html
	test -d $(INSTALL_DIR) || exit
	test -d $(INSTALL_DIR)/$(APP_NAME) || mkdir $(INSTALL_DIR)/$(APP_NAME)
	cp -r $^ $(INSTALL_DIR)/$(APP_NAME)/
	touch $@

install: .make_install

oci_build:
	$(OCI_BIN) build -f "$(OCI_FILE)" . -t "$(OCI_TAG)"

oci_run:
	$(OCI_BIN) run -dit -p "8080:80" $(OCI_TAG) && echo "Visit localhost:8080 to access the app" || echo "Did you already build the container?  Is an instance already running on the selected port?"

oci_rmi:
	$(OCI_BIN) rmi "$(OCI_TAG)" || echo "Did you build it already? If so, did you stop and rm any running containers for this image?"

uninstall:
	rm $(INSTALL_DIR)/$(APP_NAME)/{index.html,script.js,style.css}
	rmdir $(INSTALL_DIR)/$(APP_NAME)

clean:
	find . -type f -name "*~" -delete -or -name "#*#" -delete
	rm -f html/script.js
	rm -f .eslint-passed
	rm -f .stylelint-passed

.PHONY: check install oci_build oci_run oci_rmi uninstall clean
