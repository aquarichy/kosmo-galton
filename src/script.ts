/* n: 3

   ___*___
   __*_*__
   _*_*_*_
 */
interface BoardPos {
  x: number;
  y: number;
}

interface GaltonApp extends Window {
  board_pos: BoardPos;
  height: number;
  freq: { [key: number]: number };
  duration: number;
  time_start: number; /* ms */
  iterations: number;
  show_board: boolean;
  show_progress: boolean;
}

const app : GaltonApp = {
  board_pos: { x: -1, y: 0 },
  height: 0,
  freq: {},
  duration: -1,
  time_start: -1,
  iterations: 1,
  show_board: true,
  show_progress: true,
} as GaltonApp;

function galton_input_elem_get_int (elem_id : string, msg : string): number {
  const elem : HTMLInputElement = document.body.querySelector (elem_id) as HTMLInputElement;
  if (! elem.value.match (/^[0-9]+$/)) {
    window.alert (msg);
    throw new Error (msg);
  }
  return parseInt (elem.value);
}

function galton_input_elem_get_boolean (elem_id : string): boolean {
  const elem : HTMLInputElement = document.body.querySelector (elem_id) as HTMLInputElement;
  return elem.checked;
}


// eslint-disable-next-line no-unused-vars
function galton_body_onload (): void {
  galton_buttons_set_disabled (true);
}

function galton_iteration_update (cur_iter_dec : number): void {
  if (! app.show_progress) {
    return;
  }

  const cur_iter_elem : HTMLSpanElement = document.body.querySelector ("#cur-iteration") as HTMLSpanElement;
  const max_iter_elem : HTMLSpanElement = document.body.querySelector ("#max-iteration") as HTMLSpanElement;
  cur_iter_elem.innerHTML = (cur_iter_dec > -1 ? `${app.iterations - cur_iter_dec + 1}` : '0');
  max_iter_elem.innerHTML = `${app.iterations}`;
}

function galton_generate_board (cur_iter_dec : number = -1): void {
  const board_elem : HTMLDivElement = document.body.querySelector ("#galton-board") as HTMLDivElement;
  const time_elem : HTMLSpanElement = document.body.querySelector ("span#time-elapsed") as HTMLSpanElement;
  board_elem.replaceChildren ();
  time_elem.replaceChildren ();

  let board_html : string = "";

  galton_iteration_update (cur_iter_dec);

  if (app.show_board) {
    for (let y = 0; y < app.height; y++) {
      for (let k = 0; k < app.height - y; k++) {
        board_html += "&nbsp;";
      }

      for (let x = 0; x < y + 1; x++) {
        board_html += `<span class="position" id="pos-${y}-${x}" y="${y}" x="${x}">*</span>`;
        if (x < y) {
          board_html += "&nbsp;";
        }
      }

      for (let k = 0; k < app.height - y; k++) {
        board_html += "&nbsp;";
      }

      board_html += "<br />";
    }

    board_elem.innerHTML = board_html;
  }
}

function galton_generate_histogram () {
  const histogram_tbody : HTMLTableSectionElement = document.body.querySelector ("tbody#galton-histogram-tbody") as HTMLTableSectionElement;

  histogram_tbody.replaceChildren ();

  for (let x = 0; x < app.height; x++) {
    histogram_tbody.innerHTML += `<tr><td>${x}</td><td id="hist-${x}-freq">0</td><td id="hist-${x}-stars"></td></tr>`;
  }
}

// eslint-disable-next-line no-unused-vars
function galton_generate (): void {
  app.height = galton_input_elem_get_int ("input#height", "Uh oh, height should be a positive integer.");
  app.board_pos = { x: 0, y: -1 };
  app.freq = {};
  app.duration = galton_input_elem_get_int ("input#duration", "Duration must be a positive integer representing milliseconds between steps for Run");
  app.iterations = galton_input_elem_get_int ("input#iterations", "Iterations must be a positive integer ");
  app.time_start = -1;
  app.show_progress = galton_input_elem_get_boolean ("input#show-progress");
  app.show_board = galton_input_elem_get_boolean ("input#show-board");

  galton_generate_board ();
  galton_generate_histogram ();
  galton_buttons_set_disabled (false);
}

function galton_step (): boolean {
  app.board_pos.y += 1;

  if (app.board_pos.y > 0) {
    if (0.5 < Math.random ()) {
      app.board_pos.x += 1;
    }
  }

  if (app.show_board) {
    const node_id = `span#pos-${app.board_pos.y}-${app.board_pos.x}`
    const node : HTMLSpanElement | null = document.body.querySelector (node_id)!;
    if (!node) {
      window.alert (`No node found with id ${node_id}!`);
      return false;
    }
    node.innerHTML = "O";
  }

  if (app.board_pos.y >= app.height - 1) {
    /* at the bottom! */
    app.freq[app.board_pos.x] = (app.freq[app.board_pos.x] ?? 0) + 1;
    galton_histogram_update ();
    galton_buttons_set_disabled (true);
    return false;
  }

  return true;
}

function galton_buttons_set_disabled (disabled : boolean): void {
  for (const button_id of [ "step-button", "run-button", "run-many-button" ]) {
    const button : HTMLInputElement = document.body.querySelector (`input#${button_id}`) as HTMLInputElement;
    button.disabled = disabled;
  }
}

function galton_time_update (): void {
  if (app.time_start > -1) {
    const time_end : number = (new Date ()).getTime ();
    const time_elem : HTMLSpanElement = document.body.querySelector ("span#time-elapsed") as HTMLSpanElement;
    time_elem.innerHTML = `${time_end - app.time_start}ms elapsed`;
  }
}

function galton_run_setup (iter : number) {
  galton_buttons_set_disabled (true);
  app.board_pos = { x: 0, y: -1 }; /* reset board_pos (sorry manual steps, can't quick finish) */

  if (app.show_board) {
    galton_generate_board (iter); /* regenerate the board, useful for Run Many */
  } else {
    galton_iteration_update (iter); /* called within generate_board normally */
  }
}

async function galton_run_async (iter : number = 1): Promise<boolean> {
  if (iter <= 0) {
    galton_finish ();
    return false;
  }

  galton_run_setup (iter);

  for (let y = 0; y < app.height; y++) {
    galton_step ();
  }

  if (app.show_progress && (iter % Math.floor (app.iterations / 50) == 0 || iter == app.iterations)) {
    await new Promise(resolve => requestAnimationFrame(resolve));
  }
  await galton_run_async (iter - 1);

  return true;
}

function galton_run_loop (iter : number = 1): void {
  if (iter <= 0) {
    galton_finish ();
    return;
  }

  galton_run_setup (iter);

  for (let y = 0; y < app.height; y++) {
    galton_step ();
  }

  galton_run_loop (iter - 1);
}

function galton_run (iter : number = 1): void {
  if (iter <= 0) {
    galton_finish ();
    return;
  }

  galton_run_setup (iter);

  let y : number = 0;

  const interval_id : number = setInterval (() => {
    if (y >= app.height || !galton_step ()) {
      /* we're done our run, try the next */
      clearInterval (interval_id);
      setTimeout (() => { galton_run (iter - 1); }, app.duration);
    }
    y++;
  }, app.duration);
}

function galton_finish () {
  galton_time_update ();

  if (!app.show_progress) {
    app.show_progress = true;
    galton_histogram_update ();
    galton_iteration_update (1);
  }
}

// eslint-disable-next-line no-unused-vars
function galton_run_many (iterations : number = -1): void {
  if (iterations > 0) {
    app.iterations = iterations;
  }

  app.time_start = (new Date ()).getTime ();
  if (app.duration == 0) {
    if (app.show_progress) {
      galton_run_async (app.iterations);
    } else {
      galton_run_loop (app.iterations);
    }
  } else {
    galton_run (app.iterations);
  }
}

function galton_histogram_update () {
  if (!app.show_progress) {
    return;
  }

  const histogram : HTMLElement = document.body.querySelector ("#galton-histogram")!;

  for (const x of Object.keys (app.freq).map (x => parseInt (x))) {
    const x_freq : HTMLTableCellElement = histogram.querySelector (`TD#hist-${x}-freq`) as HTMLTableCellElement;
    const x_stars : HTMLTableCellElement = histogram.querySelector (`TD#hist-${x}-stars`) as HTMLTableCellElement;

    const cur_freq : number = parseInt (x_freq.innerHTML) || 0;
    x_freq.innerHTML = `${app.freq[x]}`;
    // x_stars.replaceChildren ();
    for (let i = 0; i < app.freq[x] - cur_freq; i++) {
      x_stars.innerHTML += "*";
    }
  }
}
